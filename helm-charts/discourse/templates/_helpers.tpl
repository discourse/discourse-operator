{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "discourse.fullname" -}}
{{- printf "%s" .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "app.fullname" -}}
{{- $name := .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Truncate naming resource convention for resources.
There are several resources like deployments, services that cannot be named the same
as discourse.fullname, hence we create separate naming conventions for them.
truncated name
*/}}
{{- define "discourse.webappDeploymentName" -}}
{{- $fullname := include "discourse.fullname" . }}
{{- printf "webapp-%s" $fullname | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "discourse.redisDeploymentName" -}}
{{- $fullname := include "discourse.fullname" . }}
{{- printf "redis-%s" $fullname | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "discourse.acmeAccountPrivateKeySecretName" -}}
{{- $fullname := include "discourse.fullname" . }}
{{- printf "pk-%s" $fullname | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "discourse.tlsSecretName" -}}
{{- $fullname := include "discourse.fullname" . }}
{{- printf "tls-%s" $fullname | trunc 63 | trimSuffix "-" }}
{{- end }}
