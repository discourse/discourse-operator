apiVersion: v1
kind: Namespace
metadata:
  labels:
    control-plane: controller-manager
  name: system
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: controller-manager
  namespace: system
  labels:
    control-plane: controller-manager
spec:
  selector:
    matchLabels:
      control-plane: controller-manager
  replicas: 1
  template:
    metadata:
      labels:
        control-plane: controller-manager
      annotations:
        kubectl.kubernetes.io/default-container: manager
    spec:
      securityContext:
        runAsNonRoot: true
      containers:
      - args:
        - --leader-elect
        - --leader-election-id=discourse-operator
        image: controller:latest
        imagePullPolicy: Always
        name: manager
        # When set it, these environment variables will be present at the manager's pod.
        # They will be overwritten by the ones set in the Subscription config.
        # That's why we use configMapKeyRef, referencing the discourse-operator-configuration configmap
        # created from the okd4-install cluster configuration.
        # c.f.: https://github.com/operator-framework/operator-lifecycle-manager/blob/master/doc/design/subscription-config.md
        env:
        # WILDCARD_DELEGATED_DOMAINS_REGEX is a regex that will determine whether a hostname need to request a certificate from LE
        # or not. It expects a match against the delegated domains handled by the OKD infrastructure at CERN.
        # c.f.: https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/master/docs/computing-resources/global-resources.md#dns-delegated-domains
        # It must be in the form of:
        # ^([a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*\.)(onesharedsubdomain\.web\.cern\.ch|two\.app\.cern\.ch|...){1}$
        # See README.md file > Configuration section for further information.
        - name: WILDCARD_DELEGATED_DOMAINS_REGEX
          valueFrom:
            configMapKeyRef:
              key: WILDCARD_DELEGATED_DOMAINS_REGEX
              name: discourse-operator-configuration
        # The ACME server URL. It defaults to empty value, i.e., a self-signed certificate
        # Will be overwritten per environment cluster (dev, staging, production) from the operator's Subscription.
        - name: ACME_SERVER
          valueFrom:
            configMapKeyRef:
              key: ACME_SERVER
              name: discourse-operator-configuration
        securityContext:
          allowPrivilegeEscalation: false
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8081
          initialDelaySeconds: 15
          periodSeconds: 20
        readinessProbe:
          httpGet:
            path: /readyz
            port: 8081
          initialDelaySeconds: 5
          periodSeconds: 10
        resources:
          limits:
            cpu: '1'
            memory: 4Gi
          requests:
            cpu: 500m
            memory: 2Gi
      serviceAccountName: controller-manager
      terminationGracePeriodSeconds: 10
